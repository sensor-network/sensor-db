CREATE TABLE IF NOT EXISTS sensor_data (
    timestamp BIGINT,
    room VARCHAR(255),
    temperature FLOAT,
    humidity FLOAT
);