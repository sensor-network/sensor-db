# Builder
FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

# Runner
FROM openjdk:11-slim-bullseye
COPY --from=build /home/app/target/sensordbcontroller-0.0.1-SNAPSHOT.jar /usr/local/lib/sensordbcontroller.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/sensordbcontroller.jar"]
