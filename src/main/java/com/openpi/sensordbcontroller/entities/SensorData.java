package com.openpi.sensordbcontroller.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class SensorData {

    private final long timestamp;
    private final String room;
    private final double temperature;
    private final double humidity;

}
