package com.openpi.sensordbcontroller.domain;

import com.openpi.sensordbcontroller.entities.SensorData;
import lombok.Getter;

import java.util.Date;
import java.util.Calendar;
import java.util.TimeZone;


@Getter
public class SensorDataDomain{

    SensorData sensorData;

    private Calendar calendar;
    private int total;

    public SensorDataDomain(SensorData sensorData, int total){
        this.sensorData = sensorData;
        generateCalendar();
        this.total = total;
    }

    private void generateCalendar(){
        Date date = new Date(getSensorData().getTimestamp()*1000);
        this.calendar = Calendar.getInstance();
        this.calendar.setTime(date);
        this.calendar.setTimeZone(TimeZone.getDefault());
    }
}
