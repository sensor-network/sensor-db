package com.openpi.sensordbcontroller.controller;

import com.openpi.sensordbcontroller.entities.SensorData;
import com.openpi.sensordbcontroller.services.SensorDataService;
import com.openpi.sensordbcontroller.domain.SensorDataDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
public class SensorDataController {

    @Autowired
    SensorDataService sensorDataService;

    @GetMapping("insert")
    public SensorData sensorData(@RequestParam(required = false) Long timestamp,
                                 @RequestParam(required = true) String room,
                                 @RequestParam(required = true) Double temperature,
                                 @RequestParam(required = true) Double humidity){
        if(timestamp == null){
            timestamp = System.currentTimeMillis();
        }

        SensorData sensorData = new SensorData(timestamp, room, temperature, humidity);
        sensorDataService.insert(sensorData);
        return sensorData;
    }

    @GetMapping("all")
    public List<SensorData> getAll(@RequestParam(required = false) String room){
        return sensorDataService.getAll(room);
    }

    @GetMapping("latest")
    public List<SensorDataDomain> getLatest(@RequestParam(required = false) String room){
        return sensorDataService.getLatest(room);
    }
}
