package com.openpi.sensordbcontroller.services;

import com.openpi.sensordbcontroller.entities.SensorData;
import com.openpi.sensordbcontroller.domain.SensorDataDomain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.List;

@Slf4j
@SpringBootApplication
public class SensorDataService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insert(SensorData sensorData){
        String sql = "INSERT INTO sensor_data (timestamp, room, temperature, humidity) VALUES (?, ?, ?, ?)";
        int result = jdbcTemplate.update(sql, sensorData.getTimestamp(), sensorData.getRoom(), sensorData.getTemperature(), sensorData.getHumidity());
        if (result > 0) {
            log.info("Insert " + sensorData);
        }
    }

    public List<SensorData> getAll(String room) {
        String sql = "SELECT * FROM sensor_data";
        if(room != null){
            sql += " WHERE room='" + room + "'";
        }

        List<SensorData> resultSet = jdbcTemplate.query(sql, (rs, rowNum) ->
                new SensorData(
                        rs.getLong("timestamp"),
                        rs.getString("room"),
                        rs.getDouble("temperature"),
                        rs.getDouble("humidity")
                ));

        String logMessage = "Return all " + resultSet.size() + " data points";
        if(room != null) {
            logMessage += " of " + room;
        }
        logMessage += ".";
        log.info(logMessage);

        return resultSet;
    }

    public List<SensorDataDomain> getLatest(String room) {
        String sql =
                "SELECT sensor_data.timestamp, sensor_data.room, sensor_data.temperature, sensor_data.humidity, aggregate.total " +
                "FROM sensor_data " +
                "RIGHT JOIN (" +
                        "SELECT max(timestamp) AS timestamp, count(room) AS total, room FROM sensor_data GROUP BY room) AS aggregate " +
                        "ON sensor_data.timestamp=aggregate.timestamp AND sensor_data.room=aggregate.room";
        if(room != null) {
            List<String> rooms = Arrays.asList(room.split(","));
            sql += " WHERE sensor_data.room='" + rooms.get(0) + "'";
            for(int i=1; i<rooms.size(); i++){
                sql += " OR sensor_data.room='" + rooms.get(i) + "'";
            }
        }
        sql += " ORDER BY sensor_data.room ASC";

        List<SensorDataDomain> resultSet = jdbcTemplate.query(sql, (rs, rowNum) ->
                getSensorDataDomain(
                        rs.getLong("timestamp"),
                        rs.getString("room"),
                        rs.getDouble("temperature"),
                        rs.getDouble("humidity"),
                        rs.getInt("total")
                ));

        for (SensorDataDomain sensorDataDomain : resultSet){
            log.info("Return " + sensorDataDomain.getSensorData());
        }

        return resultSet;
    }

    public SensorDataDomain getSensorDataDomain(long timestamp, String room, double temperature, double humidity, int total){
        SensorData sensorData = new SensorData(timestamp, room, temperature, humidity);
        SensorDataDomain sensorDataDomain = new SensorDataDomain(sensorData, total);
        return sensorDataDomain;
    }
}
